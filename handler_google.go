package main

import (
	"context"
	"encoding/json"
	"html/template"
	"io/ioutil"
	"net/http"

	"bitbucket.org/alimasyhur/ali_masyhur/models"
	"github.com/gorilla/sessions"
)

func getLoginURL(state string) string {
	return conf.AuthCodeURL(state)
}

//AuthHandler handles authentication
func AuthHandler(response http.ResponseWriter, request *http.Request) {
	store, err := store.Get(request, "session")
	if err != nil {
		httpError(response, err, "Getting store")
		return
	}
	query := request.URL.Query()
	retrievedState := store.Values["state"]
	if retrievedState != query.Get("state") {
		httpError(response, err, "Getting state from store")
		return
	}

	//token exchange
	token, err := conf.Exchange(context.Background(), query.Get("code"))
	if err != nil {
		httpError(response, err, "token bit")
		return
	}

	client := conf.Client(context.Background(), token)
	email, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		httpError(response, err, "email bit")
		return
	}
	defer email.Body.Close()
	data, _ := ioutil.ReadAll(email.Body)
	authUser := &models.AuthUser{}
	json.Unmarshal(data, &authUser)

	user := &models.User{Email: authUser.Email}

	//check exist user on user
	if err := user.Get(); err != nil {
		//check exist email on profile, redirect to /register to complete the registration
		profile := &models.Profile{Email: authUser.Email}
		if err := profile.Get(); err != nil {
			//store new session
			store.Values["user"] = authUser
			store.Values["name"] = authUser.Fullname
			store.Values["username"] = authUser.Email
			store.Save(request, response)

			http.Redirect(response, request, "/register", 301)
			return
		}
		//setup sessions for user using google authentication
		store.Values["user"] = authUser
		store.Values["name"] = authUser.Fullname
		store.Values["username"] = authUser.Email
		store.Save(request, response)
		//redirect to /user authenticated page
		http.Redirect(response, request, "/user", 301)
		return
	}

	http.Redirect(response, request, "/", 301)
}

//LogoutHandler handles logout
func LogoutHandler(response http.ResponseWriter, request *http.Request) {

	store, err := store.Get(request, "session")
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}

	err = store.Save(request, response)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(response, request, "/", 302)
}

//LoginHandler handles login
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session")
	if err != nil {
		httpError(w, err, "getting session")
		return
	}

	username := session.Values["username"]
	if username != nil {
		http.Redirect(w, r, "/user", 302)
		return
	}

	state = randToken()
	store, err := store.Get(r, "session")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	store.Values["state"] = state
	store.Save(r, w)

	loginURL := struct{ URL string }{}
	loginURL.URL = getLoginURL(state)
	templ := template.Must(template.ParseFiles("./templates/login.html"))
	templ.Execute(w, loginURL)
}

//SignupHandler handles sign up function
func SignupHandler(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session")
	if err != nil {
		httpError(w, err, "getting session")
		return
	}

	username := session.Values["username"]

	if username != nil {
		http.Redirect(w, r, "/user", 302)
		return
	}

	state = randToken()
	store, err := store.Get(r, "session")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	store.Values["state"] = state
	store.Save(r, w)

	loginURL := struct{ URL string }{}
	loginURL.URL = getLoginURL(state)
	templ := template.Must(template.ParseFiles("./templates/signup.html"))
	templ.Execute(w, loginURL)
}

//UserHandler handles user
func UserHandler(response http.ResponseWriter, request *http.Request) {
	session, err := store.Get(request, "session")
	if err != nil {
		httpError(response, err, "getting session")
		return
	}

	username := session.Values["username"]

	if username == nil {
		http.Redirect(response, request, "/", 302)
		return
	}

	err = models.FindProfile(username.(string))
	if err != nil {
		http.Redirect(response, request, "/register", 301)
		return
	}

	templ := template.Must(template.ParseFiles("./templates/user.html"))
	templ.Execute(response, nil)
}
