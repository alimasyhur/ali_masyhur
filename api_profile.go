package main

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/alimasyhur/ali_masyhur/models"
)

//CreateProfile endpoint
// CreateProfile executes a query typically a SELECT.
// return response OK if success create new profile
func createProfile(resp http.ResponseWriter, req *http.Request) {
	store, _ := store.Get(req, "session")
	username := store.Values["username"]
	if username == nil {
		responseWithMessage(resp, http.StatusUnauthorized, "UnAuthorized User")
		return
	}

	var p models.Profile

	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&p); err != nil {
		responseWithMessage(resp, http.StatusBadRequest, err.Error())
		return
	}

	defer req.Body.Close()

	//set profile email is same as authenticated user
	p.Email = username.(string)
	//validate Profile struct
	if validErrs := p.Validate(); len(validErrs) > 0 {
		err := map[string]interface{}{
			"error":           http.StatusAccepted,
			"validationError": validErrs,
		}
		resp.Header().Set("Content-type", "applciation/json")
		resp.WriteHeader(http.StatusAccepted)
		json.NewEncoder(resp).Encode(err)
		return
	}

	//create Profile
	if err := p.Create(); err != nil {
		responseWithMessage(resp, http.StatusAccepted, err.Error())
		return
	}

	responseWithMessage(resp, http.StatusOK, "Profile Successfully Saved")
}

//GetProfile endpoint
//GetProfile get current authenticated User
// Returns Profile Data
func getProfile(resp http.ResponseWriter, req *http.Request) {
	store, _ := store.Get(req, "session")
	username := store.Values["username"]
	if username == nil {
		responseWithMessage(resp, http.StatusUnauthorized, "UnAuthorized User")
		return
	}

	p := models.Profile{Email: username.(string)}
	if err := p.Get(); err != nil {
		responseWithMessage(resp, http.StatusInternalServerError, err.Error())
		return
	}
	response, _ := json.Marshal(p)
	resp.Write([]byte(response))
}

//UpdateProfile endpoint
//UpdateProfile executes update profile current authenticated User
func updateProfile(resp http.ResponseWriter, req *http.Request) {
	//update Profile endpoint require user session
	store, _ := store.Get(req, "session")
	username := store.Values["username"]
	if username == nil {
		responseWithMessage(resp, http.StatusUnauthorized, "UnAuthorized User")
		return
	}

	var p models.Profile

	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&p); err != nil {
		responseWithMessage(resp, http.StatusBadRequest, err.Error())
		return
	}

	defer req.Body.Close()
	p.TempEmail = username.(string)

	//validate Profile
	if validErrs := p.Validate(); len(validErrs) > 0 {
		err := map[string]interface{}{
			"code":            http.StatusAccepted,
			"validationError": validErrs,
		}
		responseWithJSON(resp, http.StatusAccepted, err)
		return
	}

	//check if email logged in user is the same as edited email
	newEmail := p.Email
	if username.(string) != p.Email {
		u := models.User{Email: p.Email}
		//check if user exist
		if err := u.Get(); err != nil {
			//check profile if email is already taken!
			if err := models.FindProfile(p.Email); err == nil {
				responseWithMessage(resp, http.StatusAccepted, "Email is already taken. Use Other Email!")
				return
			}

			//update Profile
			if err := p.Update(); err != nil {
				//return response if fail update profile
				responseWithMessage(resp, http.StatusAccepted, err.Error())
				return
			}
			//Set New Session with New Email
			store.Values["username"] = newEmail
			store.Save(req, resp)
			responseWithMessage(resp, http.StatusOK, "Success Update Profile!")
			return
		}

		//return response if user exist
		responseWithMessage(resp, http.StatusAccepted, "Email is already taken. Use Other Email!")
		return
	}

	//Update Profile User
	if err := p.Update(); err != nil {
		//return response
		responseWithMessage(resp, http.StatusAccepted, err.Error())
		return
	}

	responseWithMessage(resp, http.StatusOK, "Success Update Profile!")
}
