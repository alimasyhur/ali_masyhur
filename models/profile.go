package models

import (
	"net/url"
	"regexp"
	"strconv"
	"time"
)

//Profile contains user data provided by google
type Profile struct {
	ID        string `json:"id"`
	Fullname  string `json:"fullname"`
	Email     string `json:"email"`
	TempEmail string `json:"temp_email"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
	AuthType  int    `json:"auth_type"`
}

//Validate Profile Fields
func (p *Profile) Validate() url.Values {
	errs := url.Values{}

	// check if the fullname empty
	if p.Fullname == "" {
		errs.Add("fullname", "The Fullname field is required!")
	}

	if p.Email == "" {
		errs.Add("email", "The email field is required!")
	}

	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	if !re.MatchString(p.Email) {
		errs.Add("email", "Invalid Email Format!")
	}

	if p.Address == "" {
		errs.Add("address", "The address field is required!")
	}

	if p.Telephone == "" {
		errs.Add("telephone", "The telephone field is required!")
	}

	return errs
}

//FindProfile is a method to get one kitab based on ID params
func FindProfile(email string) error {
	findProfile := `SELECT 
							id,
							fullname,
							email 
					FROM profile 
					WHERE 
							email=?`
	p := &Profile{}
	return db.QueryRow(findProfile, email).
		Scan(&p.ID, &p.Fullname, &p.Email)

}

//Create new Kitab method
func (p *Profile) Create() error {
	createProfile := `INSERT INTO profile 
					(
						fullname, 
						email, 
						temp_email, 
						address, 
						telephone, 
						auth_type, 
						created_at, 
						updated_at
					) 
					VALUES (?,?,?,?,?,?,?,?)`
	_, err := db.Query(createProfile,
		p.Fullname,
		p.Email,
		p.Email,
		p.Address,
		p.Telephone,
		1,
		time.Now(),
		time.Now(),
	)

	if err != nil {
		return err
	}
	return nil
}

//Update Kitab method
func (p Profile) Update() error {
	updateProfile := `UPDATE profile 
						SET 
							fullname=?,
							email=?,
							address=?,
							telephone=?,
							temp_email=?,
							updated_at=?
						WHERE 
							temp_email=?`
	_, err := db.Query(updateProfile,
		p.Fullname,
		p.Email,
		p.Address,
		p.Telephone,
		p.Email,
		time.Now(),
		p.TempEmail)

	return err
}

//Get Kitab is a method to get one kitab based on ID params
func (p *Profile) Get() error {
	getProfile := `SELECT 
						id,
						fullname,
						email,
						address,
						telephone
					FROM profile 
					WHERE 
						email=?`

	result := db.QueryRow(getProfile, &p.Email).
		Scan(&p.ID, &p.Fullname, &p.Email, &p.Address, &p.Telephone)

	return result
}

//ClearProfileTable This function will create user data
func ClearProfileTable() {
	db.Exec("DELETE FROM profile")
	db.Exec("ALTER SEQUENCE profile_id_seq RESTART WITH 1")
}

//AddProfile to bulk insert to the Profile Database
func AddProfile(count int) {
	if count < 1 {
		count = 1
	}

	for i := 0; i < count; i++ {
		createProfile := `INSERT INTO profile 
					(
						fullname, 
						email, 
						temp_email, 
						address, 
						telephone, 
						auth_type, 
						created_at, 
						updated_at
					) 
					VALUES (?,?,?,?,?,?,?,?)`
		db.Query(createProfile,
			"Profile "+strconv.Itoa(i),
			"email"+strconv.Itoa(i)+"@gmail.com",
			"email"+strconv.Itoa(i)+"@gmail.com",
			"Address "+strconv.Itoa(i),
			123456+i,
			1,
			time.Now(),
			time.Now(),
		)
	}
}
