package models

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

//Configure database connection
func Configure(DB *sql.DB) {
	db = DB
}
