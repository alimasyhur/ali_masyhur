package models

//Credentials contains creadentials to google service
type Credentials struct {
	Cid     string `json:"cid"`
	Csecret string `json:"csecret"`
}
