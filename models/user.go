package models

import (
	"net/url"
	"regexp"
	"strconv"
	"time"

	"golang.org/x/crypto/bcrypt"
)

//User contains user data provided by google
type User struct {
	ID         string `json:"id"`
	Email      string `json:"email"`
	Password   string `json:"password"`
	ResetToken string `json:"reset_token"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

//Validate User Fields
func (u *User) Validate() url.Values {
	errs := url.Values{}

	if u.Email == "" {
		errs.Add("email", "The email field is required!")
	}

	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	if re.MatchString(u.Email) {
	} else {
		errs.Add("email", "Invalid Email Format!")
	}

	if u.Password == "" {
		errs.Add("password", "The password field is required!")
	}

	return errs
}

//FindUser is a method to get one kitab based on ID params
func FindUser(email string) error {
	findUser := `SELECT 
						id,
						email 
				FROM user 
				WHERE 
						email=?`
	u := &User{}
	result := db.QueryRow(findUser, email).
		Scan(&u.ID, &u.Email, &u.Password)

	return result
}

//Get User method
func (u *User) Get() error {
	getUser := `SELECT 
						id,
						email,
						password
				 FROM user 
				 WHERE 
				 		email=?`
	result := db.QueryRow(getUser, u.Email).
		Scan(&u.ID, &u.Email, &u.Password)

	return result
}

//GetToken User method
func (u *User) GetToken() error {
	getToken := `SELECT 
						id,
						email,
						password
				FROM user 
				WHERE 
						reset_token=?`
	result := db.QueryRow(getToken, u.ResetToken).
		Scan(&u.ID, &u.Email, &u.Password)

	return result
}

//UpdateEmail User method
func (u *User) UpdateEmail(oldEmail string) error {
	updateEmail := `UPDATE user 
					SET 
							email=?,
							updated_at=?
					WHERE 
							email=?`
	_, err := db.Query(updateEmail,
		u.Email,
		time.Now(),
		oldEmail,
	)

	return err
}

//UpdatePassword User method
func (u *User) UpdatePassword() error {
	updatePassword := `UPDATE user 
						SET 
							reset_token=?,
							password=?,
							updated_at=?
						WHERE reset_token=?`
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(u.Password), 8)
	_, err := db.Query(updatePassword,
		nil,
		hashedPassword,
		time.Now(),
		u.ResetToken,
	)

	return err
}

//UpdateToken User method
func (u *User) UpdateToken() error {
	updateToken := `UPDATE user 
					SET reset_token=?,
						updated_at=?
					WHERE email=?`
	_, err := db.Exec(updateToken,
		u.ResetToken,
		time.Now(),
		u.Email,
	)

	return err
}

//Create new User method
func (u *User) Create() error {
	createUser := `INSERT INTO user (
							email, 
							password, 
							created_at, 
							updated_at) 
					VALUES (
								?,
								?,
								?,
								?
							)`
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(u.Password), 8)

	_, err := db.Query(createUser,
		u.Email,
		hashedPassword,
		time.Now(),
		time.Now(),
	)

	if err != nil {
		return err
	}
	return nil
}

//ClearUserTable function will create user data
func ClearUserTable() {
	db.Exec("DELETE FROM user")
	db.Exec("ALTER SEQUENCE user_id_seq RESTART WITH 1")
}

//AddUser to bulk insert to the User Database
func AddUser(count int) {
	if count < 1 {
		count = 1
	}

	for i := 0; i < count; i++ {
		createUser := `INSERT INTO user (
							email, 
							password, 
							created_at, 
							updated_at) 
					VALUES (
								?,
								?,
								?,
								?
							)`

		hashedPassword, _ := bcrypt.GenerateFromPassword([]byte("password"+strconv.Itoa(i)), 8)

		db.Query(createUser,
			"email"+strconv.Itoa(i)+"@gmail.com",
			hashedPassword,
			time.Now(),
			time.Now(),
		)
	}
}
