package models

//AuthUser contains user data provided by google
type AuthUser struct {
	Fullname string `json:"name"`
	Email    string `json:"email"`
}
