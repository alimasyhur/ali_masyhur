package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"net/http"
)

//ErrorResp struct returns code and message of error
type ErrorResp struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

//responseWithMessage setup json format (error, message)
func responseWithMessage(resp http.ResponseWriter, code int, message string) {
	responseWithJSON(resp, code, ErrorResp{
		Code:    code,
		Message: message,
	})
}

//responseWithJSON setup response format, error code and json format response {code, message}
func responseWithJSON(resp http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(code)
	resp.Write(response)
}

//httpError returns reason of error in console and print error on the browser
func httpError(w http.ResponseWriter, err error, reason string) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
	return
}

//randToken function would be generate token
func randToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}
