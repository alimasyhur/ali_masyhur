package main

import (
	"html/template"
	"net/http"

	"bitbucket.org/alimasyhur/ali_masyhur/models"
	"github.com/gorilla/mux"
)

//IndexHandler handles
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session")
	if err != nil {
		httpError(w, err, "getting session")
		return
	}

	username := session.Values["username"]

	if username != nil {
		http.Redirect(w, r, "/user", 302)
		return
	}

	templ := template.Must(template.ParseFiles("./templates/index.html"))
	templ.Execute(w, nil)
}

//custom handler

//CustomLoginHandler handles custom login user
func customLoginHandler(response http.ResponseWriter, request *http.Request) {
	store, _ := store.Get(request, "session")

	username := store.Values["username"]

	if username != nil {
		http.Redirect(response, request, "/user", 302)
		return
	}

	templ := template.Must(template.ParseFiles("./templates/custom-login.html"))
	templ.Execute(response, nil)
}

//CustomSignUpHandler handles custom login user
func customSignUpHandler(response http.ResponseWriter, request *http.Request) {
	store, _ := store.Get(request, "session")

	username := store.Values["username"]

	if username != nil {
		http.Redirect(response, request, "/user", 302)
		return
	}

	templ := template.Must(template.ParseFiles("./templates/custom-signup.html"))
	templ.Execute(response, nil)
}

//ForgotPasswordHandler handles forgot password
func forgotPasswordHandler(response http.ResponseWriter, request *http.Request) {
	store, _ := store.Get(request, "session")

	username := store.Values["username"]

	if username != nil {
		http.Redirect(response, request, "/user", 302)
		return
	}

	templ := template.Must(template.ParseFiles("./templates/forgot-password.html"))
	templ.Execute(response, nil)
}

//ResetPasswordHandler handles reset password
func resetPasswordHandler(response http.ResponseWriter, request *http.Request) {
	store, _ := store.Get(request, "session")
	username := store.Values["username"]
	if username != nil {
		http.Redirect(response, request, "/user", 302)
		return
	}

	vars := mux.Vars(request)
	resetToken, _ := vars["reset_token"]

	user := models.User{ResetToken: resetToken}

	templ := template.Must(template.ParseFiles("./templates/reset-password.html"))
	templ.Execute(response, user)
}

//RegisterHandler handles custom login user
func registerHandler(response http.ResponseWriter, request *http.Request) {
	store, err := store.Get(request, "session")
	if err != nil {
		httpError(response, err, "getting session")
		return
	}

	username := store.Values["username"]

	if username == nil {
		http.Redirect(response, request, "/", 302)
		return
	}

	err = models.FindProfile(username.(string))
	if err == nil {
		profile := models.Profile{Email: username.(string)}
		if err := profile.Get(); err != nil {
			responseWithMessage(response, http.StatusAccepted, err.Error())
			return
		}
		http.Redirect(response, request, "/user", 302)
		return
	}

	templ := template.Must(template.ParseFiles("./templates/register.html"))
	templ.Execute(response, nil)
}

//UpdateProfileHandler handles custom login user
func updateProfileHandler(response http.ResponseWriter, request *http.Request) {
	store, err := store.Get(request, "session")
	if err != nil {
		httpError(response, err, "getting session")
		return
	}
	username := store.Values["username"]

	//check if username is empty
	if username == nil {
		http.Redirect(response, request, "/", 302)
		return
	}

	//check if authenticated user whether already registered
	if err := models.FindProfile(username.(string)); err != nil {
		http.Redirect(response, request, "/register", 302)
		return
	}

	templ := template.Must(template.ParseFiles("./templates/update-profile.html"))
	templ.Execute(response, nil)
}
