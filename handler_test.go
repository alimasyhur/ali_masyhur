package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/alimasyhur/ali_masyhur/models"
	"github.com/gorilla/sessions"
)

//:::::::::::::::::::
//Custom Handler Area
//:::::::::::::::::::

//TestIndexPageUnAuthenticatedUser
//This Page accessible by Non Authenticated User
//If Un-Authenticated User Access This Page, They will see the index page
func TestIndexPageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/index", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(IndexHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//TestIndexPageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestIndexPageAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/index", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(IndexHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "test@email.com"
	store.Values["username"] = "Test User"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestLoginPageUnAuthenticatedUser
//This Page accessible by Non Authenticated User
//If Un-Authenticated User Access This Page, They will see the Login page
func TestLoginPageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/custom-login", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(customLoginHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//TestLoginPageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestLoginPageAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/custom-login", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(customLoginHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "test@email.com"
	store.Values["username"] = "Test User"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestSignUpPageUnAuthenticatedUser
//This Page accessible by Non Authenticated User
//If Un-Authenticated User Access This Page, They will see the Login page
func TestSignUpPageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/custom-signup", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(customSignUpHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//TestSignUpPageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestSignUpPageAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/custom-signup", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(customSignUpHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "test@email.com"
	store.Values["username"] = "Test User"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestForgotPasswordPageUnAuthenticatedUser
//This Page accessible by Non Authenticated User
//If Un-Authenticated User Access This Page, They will see the Login page
func TestForgotPasswordPageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/forgot-password", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(forgotPasswordHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//TestForgotPasswordPageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestForgotPasswordPageAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/forgot-password", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(forgotPasswordHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "test@email.com"
	store.Values["username"] = "Test User"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestResetPasswordPageUnAuthenticatedUser
//This Page accessible by Non Authenticated User
//If Un-Authenticated User Access This Page, They will see the Login page
func TestResetPasswordPageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/reset-password", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(resetPasswordHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//TestResetPasswordPageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestResetPasswordPageAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/reset-password", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(resetPasswordHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "test@email.com"
	store.Values["username"] = "Test User"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestRegisterPageUnAuthenticatedUser
//This Page accessible by Authenticated User
//If Un-Authenticated User Access This Page, They will see the Home/Index page
func TestRegisterPageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/register", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(registerHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}
}

//TestRegisterPageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestRegisterPageAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/register", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(registerHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "test@email.com"
	store.Values["username"] = "Test User"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestUpdateProfilePageUnAuthenticatedUser
//This Page accessible by Authenticated User
//If Un-Authenticated User Access This Page, They will see the Home/Index page
func TestUpdateProfilePageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/update-profile", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfileHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}
}

//TestUpdateProfilePageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestUpdateProfilePageAuthenticatedUser(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(1)

	req, err := http.NewRequest("GET", "/update-profile", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfileHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//:::::::::::::::::::
//Google Handler Area
//:::::::::::::::::::

//TestUserPageUnAuthenticatedUser
//This Page accessible by Authenticated User
//If Un-Authenticated User Access This Page, They will see the Home/Index page
func TestUserPageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/user", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(UserHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}
}

//TestUserPageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestUserPageAuthenticatedUser(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(1)

	req, err := http.NewRequest("GET", "/user", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(UserHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestChooseSignUpMethodPageUnAuthenticatedUser
//This Page accessible by Non Authenticated User
//If Un-Authenticated User Access This Page, They will see the Login page
func TestChooseSignUpMethodPageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/signup", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(SignupHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//TestChooseSignUpMethodPageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestChooseSignUpMethodPageAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/signup", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(SignupHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "test@email.com"
	store.Values["username"] = "Test User"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestChooseLoginMethodPageUnAuthenticatedUser
//This Page accessible by Non Authenticated User
//If Un-Authenticated User Access This Page, They will see the Login page
func TestChooseLoginMethodPageUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/login", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LoginHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//TestChooseLoginMethodPageAuthenticatedUser
//This Page accessible by Non Authenticated User
//If User Authenticated, They will be redirected to another page
func TestChooseLoginMethodPageAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/login", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LoginHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "test@email.com"
	store.Values["username"] = "Test User"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestLogoutUnAuthenticatedUser
//This Page accessible by Non Authenticated User
//If Un-Authenticated User Access This Page, They will see the Login page
func TestLogoutUnAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/logout", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LogoutHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

	//Check Accessing User Route
	req, err = http.NewRequest("GET", "/user", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(UserHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

}

//TestLogoutAuthenticatedUser
//This Page accessible by Non Authenticated User
//If Un-Authenticated User Access This Page, They will see the Login page
func TestLogoutAuthenticatedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/logout", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LogoutHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusFound)
	}

	//Check Accessing User Route
	req, err = http.NewRequest("GET", "/user", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(UserHandler)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}
