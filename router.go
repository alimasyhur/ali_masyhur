package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

//Start Router
func Start() {
	r := mux.NewRouter()
	//Handler page
	r.HandleFunc("/", IndexHandler)
	r.HandleFunc("/login", LoginHandler)
	r.HandleFunc("/custom-login", customLoginHandler)
	r.HandleFunc("/custom-signup", customSignUpHandler)
	r.HandleFunc("/signup", SignupHandler)
	r.HandleFunc("/auth", AuthHandler)
	r.HandleFunc("/user", UserHandler)
	r.HandleFunc("/register", registerHandler)
	r.HandleFunc("/update-profile", updateProfileHandler)
	r.HandleFunc("/logout", LogoutHandler)
	r.HandleFunc("/forgot-password", forgotPasswordHandler)
	r.HandleFunc("/reset-password", resetPasswordHandler)

	//api profile
	r.HandleFunc("/api/v1/profile/get", getProfile)
	r.HandleFunc("/api/v1/profile/register", createProfile)
	r.HandleFunc("/api/v1/profile/update", updateProfile)
	//user profile
	r.HandleFunc("/api/v1/user/register", createUser)
	r.HandleFunc("/api/v1/user/login", loginUser)
	r.HandleFunc("/api/v1/user/forgot-password", forgotPassword)
	r.HandleFunc("/api/v1/user/reset-password", resetPassword)
	r.HandleFunc("/api/v1/user/get", getAuthUser)

	http.Handle("/", r)
	srv := &http.Server{
		Handler: r,
		Addr:    os.Getenv("DOMAIN") + os.Getenv("PORT"),
	}

	log.Fatal(srv.ListenAndServe())
}
