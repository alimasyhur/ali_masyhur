package main

import (
	"encoding/json"
	"net/http"
	"os"

	"bitbucket.org/alimasyhur/ali_masyhur/models"
	"golang.org/x/crypto/bcrypt"
)

//CreateUser endpoint
//CreateUser executes a query that returns rows, typically a SELECT.
func createUser(resp http.ResponseWriter, req *http.Request) {
	var u models.User
	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&u); err != nil {
		responseWithMessage(resp, http.StatusBadRequest, err.Error())
		return
	}
	defer req.Body.Close()
	//validate User
	if validErrs := u.Validate(); len(validErrs) > 0 {
		err := map[string]interface{}{
			"code":            http.StatusAccepted,
			"validationError": validErrs,
		}
		responseWithJSON(resp, http.StatusAccepted, err)
		return
	}

	p := models.Profile{Email: u.Email}

	//Check if exist email in Profile
	if err := p.Get(); err != nil {
		//Check if exist email in User
		if err := u.Get(); err != nil {
			//Execute Create New User
			if err := u.Create(); err != nil {
				responseWithMessage(resp, http.StatusAccepted, "Email is already taken. Use Other Email!")
				return
			}
			responseWithMessage(resp, http.StatusOK, "User Successfully Created")
			return
		}
		responseWithMessage(resp, http.StatusAccepted, "Email is already taken!")
		return
	}

	responseWithMessage(resp, http.StatusAccepted, "Email is already taken. Use Other Email!")
}

//loginUser endpoint
// CreateUser return response OK if use success to login
func loginUser(resp http.ResponseWriter, req *http.Request) {
	var u models.User
	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&u); err != nil {
		responseWithMessage(resp, http.StatusBadRequest, err.Error())
		return
	}
	defer req.Body.Close()
	//validate User
	if validErrs := u.Validate(); len(validErrs) > 0 {
		err := map[string]interface{}{
			"code":            http.StatusUnauthorized,
			"validationError": validErrs,
		}
		responseWithJSON(resp, http.StatusUnauthorized, err)
		return
	}

	storedUser := &models.User{Email: u.Email}

	p := &models.Profile{Email: u.Email}

	if err := storedUser.Get(); err != nil {
		if err := p.Get(); err != nil {
			responseWithMessage(resp, http.StatusAccepted, "Your Email does't Registered. Try Login Using Google Account!")
			return
		}

		responseWithMessage(resp, http.StatusAccepted, "Your Email does't Registered. Try Login Using Google Account!")
		return
	}

	//compare password between stored password on db and request posted password
	if err := bcrypt.CompareHashAndPassword([]byte(storedUser.Password), []byte(u.Password)); err != nil {
		responseWithMessage(resp, http.StatusAccepted, "Your Password is incorrect!")
		return
	}

	//stored to authenticated user
	session, err := store.Get(req, "session")
	if err != nil {
		httpError(resp, err, "getting session")
		return
	}
	//setup new session and new state
	state = randToken()
	session.Values["state"] = state
	session.Values["name"] = p.Fullname
	session.Values["username"] = p.Email
	session.Save(req, resp)

	responseWithMessage(resp, http.StatusOK, "Welcome!")
}

//GetAuthUser endpoint
func getAuthUser(resp http.ResponseWriter, req *http.Request) {
	session, _ := store.Get(req, "session")
	username := session.Values["username"]
	if username == nil {
		responseWithMessage(resp, http.StatusUnauthorized, "UnAuthorized User")
		return
	}

	u := &models.AuthUser{
		Email:    username.(string),
		Fullname: session.Values["name"].(string),
	}

	response, _ := json.Marshal(u)
	resp.Write([]byte(response))
}

//ForgotPassword endpoint
//ForgotPassword handles user who forgot theyr password
//This endpoint will send email confirmation with reset password link
func forgotPassword(resp http.ResponseWriter, req *http.Request) {
	var u models.User

	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&u); err != nil {
		responseWithMessage(resp, http.StatusBadRequest, err.Error())
		return
	}

	defer req.Body.Close()

	if err := u.Get(); err != nil {
		responseWithMessage(resp, http.StatusAccepted, "Your Email is not Registered. Try Login Using Google Account!")
		return
	}

	randomToken := string(u.Email + u.Password)
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(randomToken), 8)
	u.ResetToken = string(hashedPassword)
	if err := u.UpdateToken(); err != nil {
		responseWithMessage(resp, http.StatusAccepted, "Failed Update Reset Token")
		return
	}

	//Send Email Message Reset Password
	receiver := []string{u.Email}
	subject := "Reset Password Account"
	message := os.Getenv("BASE_URL") + `/reset-password?reset_token=` + u.ResetToken + ` is your link to reset your password`

	bodyMessage := sender.WritePlainEmail(receiver, subject, message)
	if err := sender.SendMail(receiver, subject, bodyMessage); err != nil {
		responseWithMessage(resp, http.StatusAccepted, err.Error())
		return
	}

	responseWithMessage(resp, http.StatusOK, "Check Your Email To Reset Password!")
}

//ResetPassword endpoint
//ResetPassword reset User Password with Reset Email Link
//Reset Password Link sent through email and User Open it to reset their Password
func resetPassword(resp http.ResponseWriter, req *http.Request) {
	var u models.User

	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&u); err != nil {
		responseWithMessage(resp, http.StatusBadRequest, err.Error())
		return
	}

	defer req.Body.Close()

	newPassword := u.Password

	if err := u.GetToken(); err != nil {
		responseWithMessage(resp, http.StatusAccepted, "Unknown Reset Token")
		return
	}

	//update password with new password
	u.Password = newPassword
	if err := u.UpdatePassword(); err != nil {
		responseWithMessage(resp, http.StatusAccepted, "Failed Update New Password")
		return
	}
	responseWithMessage(resp, http.StatusOK, "Success Reset Password")
}
