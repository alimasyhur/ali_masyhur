package main

import (
	"bytes"
	"database/sql"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"bitbucket.org/alimasyhur/ali_masyhur/models"
	"github.com/gorilla/sessions"
)

//:::::::::::::::::::
//API User Area
//:::::::::::::::::::

func TestMain(m *testing.M) {
	//setup database connection configuration
	db, err := sql.Open("mysql", os.Getenv("DB_DSN_TEST"))
	if err != nil {
		log.Fatalln(err)
	}

	defer db.Close()

	//Configure Database Connection
	models.Configure(db)

	code := m.Run()

	//Start Router
	os.Exit(code)
}

//TestUnauthorizeUser
func TestUnauthorizeUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/user/get", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(getAuthUser)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusUnauthorized)
	}
}

//TestAuthorizedUser
func TestAuthorizedUser(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/user/get", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(getAuthUser)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "test@email.com"
	store.Values["username"] = "Test User"
	store.Save(req, rr)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	//removes sessions
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	store.Save(req, rr)
}

//TestCreateUser
//This Function test will test User registration and save the records to the user table
func TestCreateUser(t *testing.T) {
	models.ClearUserTable()

	payload := []byte(`{"email":"test@email.com","password":"test-123"}`)

	req, err := http.NewRequest("POST", "/api/v1/user/register", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}

//TestCreateUserWithExistingEmail
//This Function test will test User registration and save the records to the user table
func TestCreateUserWithExistingEmail(t *testing.T) {
	models.ClearUserTable()
	models.AddUser(1)

	payload := []byte(`{"email":"email0@gmail.com","password":"password"}`)

	req, err := http.NewRequest("POST", "/api/v1/user/register", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestCreateUserWithEmptyEmail
//This Function test will test User registration and save the records to the user table
func TestCreateUserWithEmptyEmail(t *testing.T) {
	models.ClearUserTable()

	payload := []byte(`{"email":"","password":"password"}`)

	req, err := http.NewRequest("POST", "/api/v1/user/register", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestCreateUserWithInvalidEmailFormat
//This Function test will test User registration and save the records to the user table
func TestCreateUserWithInvalidEmailFormat(t *testing.T) {
	models.ClearUserTable()

	payload := []byte(`{"email":"email.email.com","password":"password"}`)

	req, err := http.NewRequest("POST", "/api/v1/user/register", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestCreateUserWithEmptyPassword
//This Function test will test User registration and save the records to the user table
func TestCreateUserWithEmptyPassword(t *testing.T) {
	models.ClearUserTable()

	payload := []byte(`{"email":"email@gmail.com","password":""}`)

	req, err := http.NewRequest("POST", "/api/v1/user/register", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestCreateUserWithEmptyFields
//This Function test will test User registration and save the records to the user table
func TestCreateUserWithEmptyFields(t *testing.T) {
	models.ClearUserTable()

	payload := []byte(`{"email":"","password":""}`)

	req, err := http.NewRequest("POST", "/api/v1/user/register", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestLoginUserWithUnknownEmail
//This Function test will test User registration and save the records to the user table
func TestLoginUserWithUnknownEmail(t *testing.T) {
	models.ClearUserTable()
	models.AddUser(1)

	payload := []byte(`{"email":"email1@gmail.com","password":"password0"}`)

	req, err := http.NewRequest("POST", "/api/v1/user/login", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(loginUser)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StateActive)
	}
}

//TestLoginUserWithIncorrectPassword
//This Function test will test User registration and save the records to the user table
func TestLoginUserWithIncorrectPassword(t *testing.T) {
	models.ClearUserTable()
	models.AddUser(1)

	payload := []byte(`{"email":"email0@gmail.com","password":"password1"}`)

	req, err := http.NewRequest("POST", "/api/v1/user/login", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(loginUser)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StateActive)
	}
}

//TestLoginUserSuccess
//This Function test will test User registration and save the records to the user table
func TestLoginUserSuccess(t *testing.T) {
	models.ClearUserTable()
	models.AddUser(1)

	payload := []byte(`{"email":"email0@gmail.com","password":"password0"}`)

	req, err := http.NewRequest("POST", "/api/v1/user/login", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(loginUser)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//:::::::::::::::::::
//API Profile Area
//:::::::::::::::::::

//TestUnauthorizeCreateProfile
func TestUnauthorizeCreateProfile(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/profile/register", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createProfile)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusUnauthorized)
	}
}

//TestAuthorizeCreateProfile
func TestAuthorizeCreateProfile(t *testing.T) {
	models.ClearProfileTable()

	payload := []byte(`{"fullname":"Testing","email":"jegrag4ever@gmail.com","address":"Address","telephone":"312342"}`)
	req, err := http.NewRequest("POST", "/api/v1/profile/register", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createProfile)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Test User"
	store.Values["username"] = "test@email.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//TestUnauthorizeUpdateProfile
func TestUnauthorizeUpdateProfile(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/profile/update", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfile)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusUnauthorized)
	}
}

//TestAuthorizeCreateProfile
func TestAuthorizeUpdateProfile(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(1)

	payload := []byte(`{"fullname":"Testing","email":"jegrag4ever@gmail.com","address":"Address","telephone":"312342"}`)
	req, err := http.NewRequest("POST", "/api/v1/profile/update", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfile)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//TestUpdateProfileAndEmailAlreadyUsed
func TestUpdateProfileAndEmailAlreadyUsed(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(2)

	payload := []byte(`{"fullname":"Testing","email":"email1@gmail.com","address":"Address","telephone":"312342"}`)
	req, err := http.NewRequest("POST", "/api/v1/profile/update", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfile)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestUpdateProfileEmptyFullname
func TestUpdateProfileEmptyFullname(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(1)

	payload := []byte(`{"fullname":"","email":"email0@gmail.com","address":"Address","telephone":"312342"}`)
	req, err := http.NewRequest("POST", "/api/v1/profile/update", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfile)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestUpdateProfileEmptyEmail
func TestUpdateProfileEmptyEmail(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(1)

	payload := []byte(`{"fullname":"Testing","email":"","address":"Address","telephone":"312342"}`)
	req, err := http.NewRequest("POST", "/api/v1/profile/update", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfile)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestUpdateProfileEmptyAddress
func TestUpdateProfileEmptyAddress(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(1)

	payload := []byte(`{"fullname":"Testing","email":"email@gmail.com","address":"","telephone":"312342"}`)
	req, err := http.NewRequest("POST", "/api/v1/profile/update", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfile)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestUpdateProfileEmptyTelephone
func TestUpdateProfileEmptyTelephone(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(1)

	payload := []byte(`{"fullname":"Testing","email":"email@gmail.com","address":"Address","telephone":""}`)
	req, err := http.NewRequest("POST", "/api/v1/profile/update", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfile)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestUpdateProfileEmptyFields
func TestUpdateProfileEmptyFields(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(1)

	payload := []byte(`{"fullname":"","email":"","address":"","telephone":""}`)
	req, err := http.NewRequest("POST", "/api/v1/profile/update", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(updateProfile)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}

//TestUnauthorizeGetProfile
func TestUnauthorizeGetProfile(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/profile/get", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(getProfile)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusUnauthorized)
	}
}

//TestAuthorizeGetProfile
func TestAuthorizeGetProfile(t *testing.T) {
	models.ClearProfileTable()
	models.AddProfile(1)

	req, err := http.NewRequest("GET", "/api/v1/profile/get", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(getProfile)

	store, _ := store.Get(req, "session")
	store.Values["name"] = "Profile 0"
	store.Values["username"] = "email0@gmail.com"
	store.Save(req, rr)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}
