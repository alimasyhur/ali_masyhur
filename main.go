package main

import (
	"database/sql"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/alimasyhur/ali_masyhur/helpers"
	"bitbucket.org/alimasyhur/ali_masyhur/models"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var cred models.Credentials
var conf *oauth2.Config
var state string
var store = sessions.NewCookieStore([]byte("something-very-secret"))
var sender = helpers.NewSender("alimasyhur@student.uns.ac.id", "nzq138zs44")

func init() {
	//Setup Environment
	os.Setenv("BASE_URL", "https://alimasyhur.herokuapp.com")
	os.Setenv("DOMAIN", ":")
	os.Setenv("DB_DSN", "sql12231268:vUEZiLaEKe@(sql12.freemysqlhosting.net:3306)/sql12231268?charset=utf8&parseTime=true")
	os.Setenv("DB_DSN_TEST", "sql12232724:tK8qUyLk2g@(sql12.freemysqlhosting.net:3306)/sql12232724?charset=utf8&parseTime=true")
	//Session Setup
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   3600 * 8, // 8 hours
		HttpOnly: true,
	}

	gob.Register(&models.AuthUser{})
	file, err := ioutil.ReadFile("./creds.json")
	if err != nil {
		fmt.Printf("File Error: %v\n", err)
		os.Exit(1)
	}

	json.Unmarshal(file, &cred)

	conf = &oauth2.Config{
		ClientID:     cred.Cid,
		ClientSecret: cred.Csecret,
		RedirectURL:  os.Getenv("BASE_URL") + "/auth",
		Scopes: []string{
			// scopes allow you to selectively choose the permissions you need access to
			// for simple login you can just use userinfo.email
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}
}

func main() {

	//setup database connection configuration
	db, err := sql.Open("mysql", os.Getenv("DB_DSN"))
	if err != nil {
		log.Fatalln(err)
	}

	defer db.Close()

	//Configure Database Connection
	models.Configure(db)

	//Start Router
	Start()
}
